/**
 * @author Adric
 * @created 2015-09-22
 * @course CSC110 MW 3:10PM
 * @due 2014-09-31
 * @version 2014.09.29
 * @brief Sorts three integers into ascending order using multiple 
 *        methods, chosen at random
 * 
 * Copyright 2014 Adric, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

import java.util.Random;

public class SortThreeNumbers 
{
	private static final int Q_COUNT_DFLT = 9;
	
	/**
	 * Prompts users to enter three numbers then sorts them
	 * using a randomly selected algorithm without relying
	 * on any "standard" sorting methods
	 */
	public static void main(String[] args) 
	{
		Random random = new Random(System.currentTimeMillis());
		int sum = 0;
		int q_count = 0;
		int max_q_count = Q_COUNT_DFLT;
		if (args.length == 1)
		{
			Integer count = StringUtils.toInteger(args[0]);
			if (count != null && count > 0)
			{
				max_q_count = count;
			}
		}
		InputParser parser = InputParser.create(System.in);
		for (; q_count < max_q_count; ++q_count)
		{
			System.out.print("Enter three integers (n n n): ");
			/*
			 * Easy way to get a populated array
			 */
			int nums[] = StringUtils.getSpacedInts(parser.getLine());
			if (nums.length != 3)
			{
				System.err.println("\nInvalid number(s) entered!");
				--q_count;
				continue;
			}
			sum += nums[0] + nums[1] + nums[2];
			/*
			 * Generate a pseudo-random number from 0-3 then call a random function
			 * (all work)
			 */
			int option = random.nextInt(4);
			switch (option)
			{
				case 0:
					printSortedSwap(nums[0], nums[1], nums[2]);
					break;
				case 1:
					printSortedLogic(nums[0], nums[1], nums[2]);
					break;
				case 2:
					printSortedMaxMin(nums[0], nums[1], nums[2]);
					break;
				case 3:
					printSortedSwitch(nums[0], nums[1], nums[2]);
					break;
				default:
					System.err.format("%nInvalid number generated: %d" + option);
					break;
			}
		}
		/*
		 * Print the sum and average
		 */
		System.out.format("%nSum of all numbers asked: %d", sum);
		System.out.format("%nAverage of the numbers entered: %s", StringUtils.fToStr(sum / (q_count*3.f)));
		
		parser.destroyClose(System.in);
		parser = null;
		System.exit(0);
	}
	
	/**
	 * Prints three sorted integers using swapping
	 */
	public static void printSortedSwap(int a, int b, int c)
	{
		int first = a;
		int second = b;
		int third = c;		
		int temp = 0;
		if ((first > second) || (second > third) || (first > third))
		{
			/*
			 * Test the first pair
			 */
			if (first > second)
			{
				temp = second;
				second = first;
				first = temp;
			}
			/*
			 * Test the second pair
			 */
			if (second > third)
			{
				temp = third;
				third = second;
				second = temp;
			}
			/*
			 * Test the first pair again (in case a > c)
			 */
			if (first > second)
			{
				temp = second;
				second = first;
				first = temp;
			}
		}
		
		System.out.format("%n%d %d %d sorted is %d %d %d\n", 
							a, b, c, first, second, third);
	}
	
	/**
	 * Prints three integers sorted using max/min calls
	 */
	public static void printSortedMaxMin(int a, int b, int c)
	{
		int first = getMin(getMin(a, b), c);
		int second = 0;
		if (b < a)
		{
			second = getMin(getMax(a, b), getMax(b, c));
		}
		else
		{
			second = getMax(getMin(a, b), getMin(b, c));
		}
		int third = getMax(getMax(a, b), c);
		
		System.out.format("%n%d %d %d sorted is %d %d %d\n", 
							a, b, c, first, second, third);
	}
	
	/**
	 * Prints three integers sorted using if/else statements
	 */
	public static void printSortedLogic(int a, int b, int c)
	{
		int first = 0;
		int second = 0;
		int third = 0;
		/*
		 * Find one place, then the other two
		 */
		if (a > c)
		{	
			if (a > b)
			{
				third = a;
				if (b > c)
				{
					second = b;
					first = c;
				}
				else // b <= c
				{
					second = c;
					first = b;
				}
			}
			else
			{
				second = a;
				if (b > c)
				{
					third = b;
					first = c;
				}
				else // b <= c
				{
					third = c;
					first = b;
				}
			}
		}
		else // a <= c
		{
			if (c > b)
			{
				third = c;
				if (b > a)
				{
					second = b;
					first = a;
				}
				else // b <= a
				{
					second = a;
					first = b;
				}
			}
			else // c <= b
			{
				second = c;
				if (b > a)
				{
					third = b;
					first = a;
				}
				else
				{
					third = a;
					first = b;
				}
			}
		}
		
		System.out.format("%n%d %d %d sorted is %d %d %d\n", 
							a, b, c, first, second, third);	}
	
	/**
	 * Prints three integers sorted with else if statements
	 */
	public static void printSortedSwitch(int a, int b, int c)
	{
		int first = 0;
		int second = 0;
		int third = 0;
		/*
		 * Selects a variant (allowing for doubles):
		 * 1. abc
		 * 2. acb
		 * 3. bac
		 * 4. bca
		 * 5. cab
		 * 6. cba
		 */
		if ((c >= b) && (b >= a) && (c >= a))
		{
			first = a;
			second = b;
			third = c;
		}
		else if ((b >= c) && (c >= a) && (b >= a))
		{
			first = a;
			second = c;
			third = b;
		}
		else if ((c >= a) && (a >= b) && (c >= b))
		{
			first = b;
			second = a;
			third = c;
		}
		else if ((a >= c) && (c >= b) && (a >= b))
		{
			first = b;
			second = c;
			third = a;
		}
		else if ((b >= a) && (a >= c) && (b >= c))
		{
			first = c;
			second = a;
			third = b;
		}
		else if ((a >= b) && (a >= c) && (b >= c))
		{
			first = c;
			second = b;
			third = a;
		}
		else
		{
			// Should never get here
			System.err.format("%nUnsorted triple in printSortedSwitch(): "
							+ "%d %d %d", a, b, c);
		}
		
		System.out.format("%n%d %d %d sorted is %d %d %d\n", 
							a, b, c, first, second, third);	}
	
	/**
	 * Returns the lesser of two values
	 */
	public static int getMin(int a, int b)
	{
		if (a < b)
		{
			return a;
		}
		return b; // b <= a
	}
	
	/**
	 * Returns the greater of two values
	 */
	public static int getMax(int a, int b)
	{
		if (a > b)
		{
			return a;
		}
		return b; // b >= a
	}
}
