/**
 * @author Adric
 * @created 2014-08-04
 * @course CSC110 MW 3:10PM
 * @due 2014-10-20
 * @version 2014.08.04
 * @brief Prints a square of input length
 *
 * This Source Code Form is subject to the terms of the Mozilla 
 * Public License, v. 2.0. If a copy of the MPL was not distributed 
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

public class PrintSquares
{
	private final static int QUIT = -1;
	private static final int MIN_LEN = 0;
	private static final int MAX_LEN = 64;
	
	/**
	 * Program main
	 */
	public static void main(String[] args)
	{
		int input_count = 0;
		int total_length = 0;
		InputParser parser = InputParser.create();
		do
		{
			System.out.print("Enter a length between " + MIN_LEN + " and " + MAX_LEN + " (inclusive) or enter " + QUIT + " to exit: ");
			int input = parser.getInt();
			if (input == QUIT)
			{
				break;
			}
			
			while ((input < MIN_LEN) || (input > MAX_LEN))
			{
				System.out.format("\nLength entered: %d is invalid.", input);
				System.out.print("\nLength must be between " + MIN_LEN + " and " + MAX_LEN + " (inclusive) or enter " + QUIT + " to exit: ");
				input = parser.getInt();
			}
			input_count++;
			total_length += input;
			printSquare(input);
		}
		while (true);
		/*
		 * Print and quit
		 */
		System.out.format("\n%d squares printed.  Average length: %f", input_count, ((input_count == 0) ? 0 : StringUtils.fToStr((float)total_length/input_count)));	    
		parser.destroyCloseAll();
		parser = null;
		System.exit(0);
	}
	
	/**
	 * Prints a single square of length, like so:
	 * 	+-+
	 *  | |
	 *  +-+
	 */
	public static void printSquare(int length)
	{
		for (int i = 1; i <= length; ++i) // Row
		{
			for (int j = 1; j <= length; ++j) // Line
			{
				if ((i == 1) || (i == length))
				{
					if ((j == 1) || (j == length))
					{
						System.out.print("+"); // Corner
					}
					else
					{
						System.out.print("-"); // Top/bottom
					}
				}
				else
				{
					if ((j == 1) || (j == length))
					{
						System.out.print("|"); // Left/right
					}
					else
					{
						System.out.print(" "); // Inside
					}
				}
			}
			System.out.print("\n");
		}
		System.out.print("\n");
	}
}
