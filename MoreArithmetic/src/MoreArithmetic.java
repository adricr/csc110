/**
 * @author Adric
 * @created 2015-09-13
 * @course CSC110 MW 3:10PM
 * @due 2014-09-24
 * @version 2014.09.13
 * @brief Convert date formats and calculate bmi/absi
 * 
 * The BSD 2-Clause License
 * 
 * Copyright (c) 2014, Adric
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other 
 * materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */

public class MoreArithmetic
{
	private int		mYear;
	private int		mMonth;
	private int		mDay;
	private int		mHeight;
	private double	mBMI;
	private double	mABSI;
	
	/**
	 * Program main
	 */
	public static void main(String[] args)
	{
		new MoreArithmetic();
	}
	
	/**
	 * This object prompts the user to enter data, then stores it for later
	 */
	public MoreArithmetic()
	{
		mYear 	= 1970;
		mMonth 	= 1;
		mDay 	= 1;
		mHeight = 0;
		mBMI 	= 0.f;
		mABSI 	= 0.f;
		
		prompt();
	}
	
	/**
	 * Prompt the user for options until they quit
	 */
	private void prompt()
	{
		/*
		 *  Simple command-line options:
		 * 	1. Enter data
		 * 	2. Exit
		 */
		boolean loop = true;
		InputParser parser = InputParser.create();
		do
		{
			System.out.println("1. Enter data");
			System.out.println("2. Exit");
			System.out.print("Select an option: ");
			int selection = parser.getInt();
			
			switch (selection)
			{
				case 1:
				{
					// Ask for date
					System.out.print("\nEnter date (YYYYMMDD): ");
					int date = parser.getInt();
					while ((date < 10101) || (date > 99991231))
					{
						System.out.print("\nDate out of range! "
								+ "Enter a valid date (YYYYMMDD): ");
						date = parser.getInt();
					}
					// Note: year is validated when the int is inputed
					mYear 	= date / 10000;
					mMonth 	= (date % 10000) / 100;
					while ((mMonth < 1) || (mMonth > 12))
					{
						System.out.print("\nMonth out of range! "
										+ "Enter a valid month (1-12): ");
						mMonth = parser.getInt();
					}
					mDay	= date % 100;
					while ((mDay < 1) || (mDay > 31))
					{
						System.out.print("\nDay out of range! "
										+ "Enter a valid day (1-31): ");
						mDay = parser.getInt();
					}
					
					/*
					 * Example output:
					 * 20380118: 1/18/2038 (U.S.) ... 18/1/2038 (most widely used)
					 */
					System.out.format("\n%d: %d/%d/%d (U.S.) ... "
									+ "%d/%d/%d (most widely used)",
									date, mMonth, mDay, mYear, mDay, mMonth, mYear);
					
					// Ask for waist circumference in inches,
					System.out.print("\n\nEnter waist circumference (inches): ");
					float waist_circum= parser.getFloat();
					while (waist_circum <= 0.f)
					{
						System.out.print("\nMust enter a positive weight circumference (inches): ");
						waist_circum = parser.getFloat();
					}
					
					// Ask for weight in pounds
					System.out.print("Enter weight (pounds): ");
					float weight = parser.getFloat();
					while (weight <= 0.f)
					{
						System.out.print("\nMust enter a positive weight (pounds): ");
						weight = parser.getFloat();
					}
					
					/* Ask for height in feet and inches.
					 * 
					 * I haven't figured out how to validate this yet.
					 * Reading the line advances the scanner to the end,
					 * likewise with findInLine.
					 * Perhaps putting readLine() in a loop and checking
					 * the result against Pattern.compile("\\d{1,}\\s++\\d{1,}")
					 * or similar? I haven't been able to get it to work without 
					 * requiring the user to input the same values twice.
					 */
					System.out.print("Enter a height (feet SPACE inches): ");
					String[] height = parser.getLine().split("\\s++");
					int feet = 0;
					int inches = 0;
					// Lots of ways the height could be invalid
					while ((height.length != 2) || !StringUtils.areIntegers(height) ||
							((feet = Integer.valueOf(height[0])) <= 0) || ((inches = Integer.valueOf(height[1])) <= 0))
					{
						System.out.print("Invalid height! Must enter a height in the form feet SPACE inches: ");
						height = parser.getLine().split("\\s++");
					}
					
					mHeight = feet*12 + inches;
	
					/*
					 * BMI  calculation = weight / height^2 * 703
					 * ABSI calculation = waist_circumference / 
					 * 					  (BMI^(2.0/3.0) * height^(0.5))
					 */
					mBMI  = weight / Math.pow(mHeight, 2.f) * 703;
					mABSI = waist_circum / (Math.pow(mBMI, (2.f/3.f)) * Math.pow(mHeight, 0.5f));
					System.out.println("\nBMI: " + StringUtils.fToStr(mBMI));
					System.out.println("\nABSI: " + StringUtils.fToStr(mABSI) + "\n");
				}
					break;
				case 2:
					System.out.print("\nGoodbye!");
					loop = false;
					break;
				default:
					System.out.print("\nInvalid selection!\n");
					break;
			}
		}
		while (loop);
		parser.destroyCloseAll();
		parser = null;
		System.exit(0);
	}
}
