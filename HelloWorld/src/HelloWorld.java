/**
 * @author Adric
 * @course CSC110 M/W 3:10 PM
 * @due 2014-09-10
 * @brief Prints my name and "Hello World" 
 * 
 * HelloWorld is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * HelloWorld is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with HelloWorld.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

public class HelloWorld 
{
	/**
	 * HelloWorld application main
	 */
	public static void main(String[] args)
	{
		System.out.println("Hello, world! This is Adric saying \"Hello, world\"");
		System.exit(0);
	}
}