/**
 * @author Adric
 * @created 2015-09-22
 * @course CSC110 MW 3:10PM
 * @due 2014-09-29
 * @version 2014.09.24
 * @brief Sort two integers into ascending order 
 * 		  then print the total sum and average
 * 
 * Copyright 2014 Adric
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on an 
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
 * either express or implied. See the License for the specific 
 * language governing permissions and limitations under the License.
 * 
 */

/**
 * Prompts the user to enter two numbers then sort them
 */
public class SortTwoNumbers
{
	private final static int Q_COUNT_DFLT = 3;

	/**
	 * Program main
	 * @param args
	 */
	public static void main(String[] args)
	{
		int sum = 0;
		int q_count = 0;
		int max_q_count = Q_COUNT_DFLT;
		if (args.length == 1)
		{
			Integer count = StringUtils.toInteger(args[0]);
			if (count != null && count > 0)
			{
				max_q_count = count;
			}
		}
		InputParser parser = InputParser.create();
		while (q_count++ < max_q_count)
		{
			System.out.print("Enter two integers (n n): ");
			
			/*
			 * Parser does the trimming
			 */
			String[] nums = parser.getLine().split("\\s++");
			if (nums.length != 2 || !StringUtils.areIntegers(nums))
			{
				/*
				 * Keep the question count from iterating if there's an error
				 */
				q_count--;
				System.err.println("Invalid inputs!");
				continue;
			}
			
			int a = Integer.parseInt(nums[0]);
			int b = Integer.parseInt(nums[1]);
			
			/*
			 * If they're the same, order doesn't matter
			 */
			if (a > b)
			{
				int temp = a;
				a = b;
				b = temp;
			}
			
			/*
			 * Print output in the form: 3 2 sorted in ascending order: 2 3
			 */
			System.out.format("%s %s sorted in ascending order: %d %d\n\n", nums[0], nums[1], a, b);
			
			sum += a+b;
		}
		System.out.format("Sum of all numbers entered: %d" +
						"\nAverage of all numbers entered: %s\n", sum, StringUtils.fToStr(sum/((q_count-1)*2.f)));
		
		parser.destroyCloseAll();
		parser = null;
		System.exit(0);
	}
}
