/**
 * Class of static String utility functions
 * @author Adric
 */
public class StringUtils
{
	/**
	 * Returns the number of times a char exists in an ASCII String
	 * @param string
	 * @param ch
	 * @return
	 */
	public static int count(String string, char ch)
	{
		int count = 0;
		for (int i = 0; i < string.length(); ++i)
		{
			if (string.charAt(i) == ch)
			{
				count++;
			}
		}
		return count;
	}
	
	/**
	 * Converts a String to a StringBuffer object
	 * @param str	If str is null, then the StringBuffer "null" is returned. 
	 * @return
	 */
	public static StringBuffer toStringBuffer(String str)
	{
		StringBuffer sb = new StringBuffer();
		return sb.append(str);
	}
	
	public static Integer toInteger(String str)
	{
		try
		{
			return Integer.valueOf(str);
		}
		catch (NumberFormatException e)
		{
			return null;
		}
	}
	
	/**
	 * Returns true if String str is an integer. 
	 * For floats converted to strings, will only return true if 
	 * all numbers after the decimal point are 0.
	 * @param str
	 * @return
	 */
	public static boolean isInteger(String str)
	{
		if (str == null) return false;
		try
		{
			// Simple test
			Integer.parseInt(str);
			return true;
		}
		catch (NumberFormatException e)
		{
			// Integer.parseInt(str) fails to catch strings like "5.0"
			// so use this method as a workaround
			try
			{
				Float.parseFloat(str);
			}
			catch (NumberFormatException e1)
			{
				return false;
			}
			// It's a float, determine if it's also an int
			int decimal = 0;
			if ((decimal = str.lastIndexOf(".")+1) != 0)
			{
				for ( ; decimal < str.length(); decimal++)
				{
					if (str.charAt(decimal) != '0')
					{
						return false;
					}
				}
			}
			return true;
		}
	}
	
	/**
	 * Returns true if all Strings in the array are integers
	 * @param strs
	 * @return
	 */
	public static boolean areIntegers(String[] strs)
	{
		for (String s : strs)
		{
			if (!isInteger(s))
			{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Returns an array of integers from an a String with integers separated by spaces (e.g. "n n n")
	 * If any of the strings are non-integers, returns an empty array
	 * @param str
	 * @return
	 */
	public static int[] getSpacedInts(String str)
	{
		if (str != null)
		{
			String[] strs = str.trim().split("\\s++");
			if (strs.length > 0)
			{
				return getInts(strs);
			}
		}
		return new int[0];
	}
	
	/**
	 * Returns an array of integers from an array of Strings
	 * If any of the Strings are non-integers, returns an empty array
	 */
	public static int[] getInts(String[] strs)
	{
		if (strs != null && strs.length > 0)
		{
			int[] ints = new int[strs.length];
			for (int i = 0; i < strs.length; ++i)
			{
				Integer in = toInteger(strs[i]);
				if (in != null)
				{
					ints[i] = in;
				}
				else
				{
					return new int[0];
				}
			}
			return ints;
		}
		return new int[0];
	}
	
	/**
	 * Returns a pretty string for outputting floating point numbers to the user,
	 * either as whole numbers or as properly rounded decimals with trailing zeroes optionally removed.
	 * For rounding higher values or displaying scientific notation, there are better options.
	 * @param num	Float to format for outputting
	 * @return 		Returns output-ready string
	 */
	public static String fToStr(double num, int decimal_places, boolean remove_trailing_zeroes)
	{
		String str = Double.toString(num);
		if (StringUtils.isInteger(str))
		{
			// I don't know why, but Integer.toString((int)num)
			// produces decimals, so using substring instead
			int decimal = str.lastIndexOf(".");
			if (decimal != -1)
			{
				return str.substring(0, decimal);
			}
			return str;
		}
		else
		{
			if (decimal_places == 0)
			{
				return String.format("%.0f", num);
			}
			else if (decimal_places > 0)
			{
				str = String.format("%."+Integer.valueOf(decimal_places)+"f", num);
				if (remove_trailing_zeroes)
				{
					str = removeTrailingZeroes(str);
				}
			}
			else
			{
				System.err.println("Invalid number of decimal places specified: " + decimal_places);
			}
			return str;
		}
	}
	
	/**
	 * Returns a pretty string for outputting floating point numbers to the user,
	 * either as whole numbers or as properly rounded decimals with trailing zeroes removed.
	 * For rounding higher values or displaying scientific notation, there are better options.
	 * Uses 6 decimal places by default.
	 * @param num
	 * @return
	 */
	public static String fToStr(double num)
	{
		return fToStr(num, 6, true);
	}
	
	public static String removeTrailingZeroes(String str)
	{
		if (str == null) return null;
		int last_num = str.length();
		for ( ; --last_num >= 0; )
		{
			if (str.charAt(last_num) != '0')
			{
				break;
			}
		}
		if (last_num != 0)
		{
			if (str.charAt(last_num) == '.')
			{
				last_num--;
			}
			return str.substring(0, last_num+1);
		}
		return str;
	}
}
