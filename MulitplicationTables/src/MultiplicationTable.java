/**
 * @author Adric
 * @created 2014-11-11
 * @course CSC110 MW 3:10PM
 * @due 2014-11-24
 * @version 2014.11.11
 * @brief Generates a multiplication table based on user input.
 * 		  Contains the following additional classes: Cell, Table
 * 
 * NTP License (NTP)
 * Copyright (c) Adric From 2014-Present
 * 
 * Permission to use, copy, modify, and distribute this software and 
 * its documentation for any purpose with or without fee is hereby 
 * granted, provided that the above copyright notice appears in all 
 * copies and that both the copyright notice and this permission 
 * notice appear in supporting documentation, and that the name 
 * not be used in advertising or publicity pertaining to distribution 
 * of the software without specific, written prior permission. 
 * 
 * Adric makes no representations about the suitability this 
 * software for any purpose. It is provided "as is" without express 
 * or implied warranty.
 */

import java.io.PrintStream;

/**
 *  Enter into a sentinel controlled query loop that prompts the user to enter how many rows and columns 
 *  they want to see in a multiplication table.
 *  
 *  Based on the user inputs, generate and print the appropriate multiplication table. 
 *  If the user enters -99 for the number of rows or the number of columns, 
 *  then print the total number of tables generated and terminate the program.
 *  
 *  Print an error message to the standard error stream if any of the following are true:
 *	 number of rows is less than 1 (and not equal the sentinel value)
 *	 number of rows is greater than 16
 *   number of columns is less than 1
 *   number of columns is greater than 12 
 *   
 *  Print headings along the top and down the side of each table.
 *  All three repetition control statements in this program 
 *  (at least one for() statement, one while() statement, and one do-while() statement) are required. 
 */


/**
 * Class to store the value of a cell as a string
 */
class CellValue implements Cloneable, Comparable<CellValue>
{
	private String mValue;
	CellValue(String value)
	{
		this.mValue = value;
	}
	public String getValue()
	{
		return mValue;
	}
	public Object clone() throws CloneNotSupportedException
	{
	    return super.clone();
	}
	/**
	 * Returns a negative integer, zero, or a positive integer as this object is 
	 * less than, equal to, or greater than the specified object
	 */
	@Override
	public int compareTo(CellValue cv)
	{
		return mValue.compareTo(cv.getValue());
	}
}

/**
 * Class to describe a cell of padded width
 * Functionality that still needs doing:
 * 	vertical padding
 * 	vertical alignment
 * 	fine-tuning center-align
 */
class Cell implements Cloneable, Comparable<Cell>
{
	public static final int L_ALIGN = -1;
	public static final int C_ALIGN = 0;
	public static final int R_ALIGN = 1;
	public static final int DEFAULT_HPAD = 6;
	public static final int MAX_HPAD = 64;
	/**
	 * Padded cell contents
	 */
	private String 	mContents;
	/**
	 * Unpadded cell contents
	 */
	private CellValue   mValue;
	private int 		mHPad;
	private int 		mAlign;
	
	Cell(String contents)
	{
		this(contents, DEFAULT_HPAD, R_ALIGN);
	}
	
	Cell(Number contents)
	{
		this(String.valueOf(contents), DEFAULT_HPAD, R_ALIGN);
	}
	
	Cell(Number contents, int padding, int align)
	{
		this(String.valueOf(contents), padding, align);
	}
	
	Cell(String contents, int padding, int align)
	{
		mValue	  = new CellValue(contents);
		mHPad 	  = (padding < 0 || padding > MAX_HPAD)  ? DEFAULT_HPAD : padding;
		mAlign 	  = (align < L_ALIGN || align > R_ALIGN) ? R_ALIGN 		: align;
		mContents = getPaddedStr(contents, mHPad, mAlign);
	}
	
	/**
	 * Copy constructor that only copies values
	 * For actual cloning, use clone()
	 */
	Cell(Cell cell)
	{
		this.mValue	   = new CellValue(cell.getValue());
		this.mContents = cell.getContents();
		this.mHPad 	   = cell.getHPadSetting();
		this.mAlign	   = cell.getAlign();
	}
	
	/**
	 * Returns the width of the cell, i.e. contents including padding
	 */
	public int getWidth()
	{
		return mContents.length();
	}
	
	/**
	 * Gets the padded contents of the cell
	 */
	public String getContents()
	{
		return mContents;
	}
	
	/**
	 * Gets the unpadded value of the cell
	 */
	public String getValue()
	{
		return mValue.getValue();
	}
	
	/**
	 * Returns true if the horizontal padding was successfully changed or remained the same
	 * Returns false if h_pad was invalid
	 */
	public boolean setHPad(int h_pad)
	{
		if (h_pad == mHPad) return true;
		
		String contents = mContents.trim();
		if (h_pad < 0 || h_pad < contents.length())
		{
			return false;
		}
		mHPad = h_pad;
		mContents = getPaddedStr(contents, h_pad, mAlign);
		
		return true;
	}
	
	/**
	 * Gets the cell's default horizontal padding size
	 * (NOT the padding between the element and the boundary)
	 * @return
	 */
	public int getHPadSetting()
	{
		return mHPad;
	}
	
	/**
	 * Returns true if the alignment was successfully changed or remained the same
	 * Returns false if align was invalid
	 */
	public boolean setAlign(int align)
	{
		if (align == mAlign) return true;
		
		if (align < L_ALIGN || align > R_ALIGN)
		{
			return false;
		}
		mContents = getPaddedStr(mContents, mHPad, align);
		mAlign = align;
		return true;
	}
	
	/**
	 * Gets the cell's text alignment
 	 */
	public int getAlign()
	{
		return mAlign;
	}
	
	/**
	 * Based on pad() by Gerald Thurman, provided for his CSC110 class
	 * Returns a left, right, or center aligned string padded with spaces
	 */
	private static String getPaddedStr(String str, int max_length, int align)
	{
		if (str == null)
		{
			str = new String(" ");
		}
		
		if (str.length() < max_length)
		{
			int offset_b = 0;
			int offset_e = str.length();
			if (align == R_ALIGN)
			{
				offset_b = max_length - str.length();
				offset_e = max_length;
			}
			else if (align == C_ALIGN)
			{
				offset_b = (max_length - str.length())/2;
				offset_e = max_length - offset_b;
				if (str.length() % 2 != 0)
				{
					offset_b++;
				}
			}
			StringBuffer str_buf = new StringBuffer();;
			for (int i = 0; i < max_length; ++i)
			{
				if (i < offset_b || i >= offset_e)
				{
					str_buf.append(' ');
				}
				else
				{
					str_buf.append(str.charAt(i - offset_b));
				}
			}
			return str_buf.toString();
		}
		return str;
	}
	
	public Object clone() throws CloneNotSupportedException
	{
	    Cell cell = (Cell)super.clone();
	    cell.mValue = (CellValue)mValue.clone();
	    return cell;
	}
	
	public void copyValueOf(Cell cell) throws CloneNotSupportedException
	{
		this.mValue = (CellValue)cell.mValue.clone();
	}

	@Override
	public int compareTo(Cell cell) 
	{
		return this.mValue.compareTo(cell.mValue);
	}
}

/**
 * 2D table of rows and columns made up of Cells
 * A Row's header is in its 0th row
 * A Column's header is in its 0th Column
 * Both are separated by a character.
 * Functionality still missing:
 * 	addColumn()
 *  addRow()
 * 	clearColumn()
 *  clearRow()
 *  shrink()
 *  clearCell()
 *  removeCell()
 */
class Table implements Cloneable
{
	public static final char UNDERLINE_CHAR_DFLT = '-';
	public static final char EDGE_CHAR_DFLT = '|';
	// Use for displaying blank cells. Don't ever assign this to an actual cell
	private static final Cell EMPTY_CELL = new Cell(" ");
	private static final Cell EDGE_CELL = new Cell(""+EDGE_CHAR_DFLT);
	private static final Cell NULL_CELL = new Cell("null");
	private boolean mHBar = true;
	private boolean mVBar = true;
	private boolean mPrintNull = false;
	
	/**
	 * Multidimensional array representing columns and rows in the form of cell[row][column]
	 * where cell[0][0] is top left corner, and the 0th row and 0th column are reserved for headers
	 */
	Cell[][] mTable;
	/**
	 * Used to track the size of the array rather than iterating through the array each time
	 */
	private int mCount;
	
	Table(int rows, int columns)
	{
		/**
		 * Create an empty table if negative rows or columns
		 */
		columns = Math.max(columns, 0);
		rows  = Math.max(rows, 0);
		mTable = new Cell[rows][columns];
	}
	
	public Cell get(int row, int column)
	{
		if (row < 0 || column < 0)
		{
			return null;
		}
		int columns = getNumColumns();
		int rows = getNumRows();
		if (row > (rows-1) || column > (columns-1))
		{
			return null;
		}
		return mTable[row][column];
	}
	
	/**
	 * Adds a Cell to the Table.
	 * Returns true if the Cell was successfully added (or overwritten if overwrite is true).
	 * Returns false if the cell was not added.
	 */
	public boolean add(Cell cell, int row, int column, boolean overwrite)
	{
		if (row < 0 || column < 0)
		{
			return false;
		}
		
		/**
		 * Resize the array if we need to
		 */
		resizeIfNeeded(row, column);
		
		/**
		 * Insert
		 */
		if (mTable[row][column] != null && !overwrite)
		{
			return false;
		}
		if (mTable[row][column] == null)
		{
			mCount++;
		}
		mTable[row][column] = cell;
		return true;
	}
	
	public boolean removeRow(int row)
	{
		/**
		 * Can't remove the 0th row
		 */
		if (row <= 0 || row > getNumRows()-1)
		{
			return false;
		}
		
		try
		{
			Cell[][] array_copy = new Cell[getNumRows()-1][getNumColumns()];
			// TODO: compact these loops!
			for (int i = 0; i < row; ++i)
			{
				for (int j = 0; j < mTable[i].length; ++j)
				{
					array_copy[i][j] = mTable[i][j];
				}
			}
			for (int i = row+1; i < mTable.length; ++i)
			{
				for (int j = 0; j < mTable[i].length; ++j)
				{
					array_copy[i-1][j] = mTable[i][j];
				}
			}
			for (int i = 0; i < mTable[row].length; ++i)
			{
				if (mTable[row][i] != null)
				{
					mCount--;
				}
			}
			mTable = array_copy;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean removeColumn(int column)
	{
		/**
		 * Can't remove the 0th column
		 */
		if (column <= 0 || column > getNumColumns()-1)
		{
			return false;
		}
		
		try
		{
			Cell[][] array_copy = new Cell[getNumRows()][getNumColumns()-1];
			// TODO: compact these loops!
			for (int i = 0; i < mTable.length; ++i)
			{
				for (int j = 0; j < column; ++j)
				{
					array_copy[i][j] = mTable[i][j];
				}
				for (int j = column+1; j < mTable[i].length; ++j)
				{
					array_copy[i][j-1] = mTable[i][j];
				}
			}
			for (int i = 0; i < mTable.length; ++i)
			{
				if (mTable[i][column] != null)
				{
					mCount--;
				}
			}
			mTable = array_copy;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	/**
	 * Resizes the two-dimensional Table array if needed.
	 * Returns false if there was an error in resizing, true if resizing was either successful or didn't happen
	 */
	private boolean resizeIfNeeded(int new_row, int new_column)
	{
		if (new_row < 0 || new_column < 0)
		{
			return false;
		}
		int rows = getNumRows()-1;
		int columns = getNumColumns()-1;
		/**
		 * Resize the array if needed
		 */
		if (new_column > columns || new_row > rows)
		{
			columns = Math.max(new_column, columns);
			rows = Math.max(new_row, rows);
			
			/**
			 * Java doesn't have a good way of copying multidimensional arrays
			 */
			try
			{
				Cell[][] array_copy = new Cell[rows+1][columns+1];
				for (int i = 0; i < mTable.length; ++i)
				{
					for (int j = 0; j < mTable[i].length; ++j)
					{
						array_copy[i][j] = mTable[i][j];
					}
				}
				mTable = array_copy;
			}
			catch (Exception e)
			{
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Sets whether or not to print the horizontal header bar
	 */
	public void setPrintHBar(boolean enabled)
	{
		mHBar = enabled;
	}
	
	/**
	 * Sets whether or not to print the vertical header bar
	 */
	public void setPrintVBar(boolean enabled)
	{
		mVBar = enabled;
	}
	
	/**
	 * Sets whether or not to print "null" for empty cells
	 */
	public void setPrintNull(boolean enabled)
	{
		mPrintNull = enabled;
	}
	
	public void clear()
	{
		for (int i = 0; i < mTable.length; ++i)
		{
			for (int j = 0; j < mTable[0].length; ++j)
			{
				mTable[i][j] = null;
			}
		}
		mCount = 0;
	}
	
	/**
	 * Returns the number of Cells in the table
	 */
	public int count()
	{
		return mCount;
	}
	
	/**
	 * Returns true if the table is empty, false if not
	 */
	public boolean isEmpty()
	{
		return (mCount == 0);
	}
	
	public int getNumRows()
	{
		if (mTable == null || mTable.length == 0)
		{
			return 0;
		}
		return mTable.length;
	}
	
	public int getNumColumns()
	{
		if (mTable != null && mTable.length > 0)
		{
			if (mTable[0] != null)
			{
				return mTable[0].length;	
			}
		}
		return 0;
	}
	
	/**
	 * Recursively build the header's underline string
	 */
	private StringBuffer getUnderlineStr(StringBuffer str_buf, char underline_ch, int length)
	{
		if (length != 0)
		{
			str_buf.append(underline_ch);
			getUnderlineStr(str_buf, underline_ch, --length);
		}
		return str_buf;
	}
	
	public void print(PrintStream out)
	{
		// TODO: get the largest cell width in each column instead of using the default cell padding size!
		for (int i = 0; i < mTable.length; ++i)
		{
			/**
			 * Print the horizontal bar
			 */
			if (i == 1 && mHBar)
			{
				/**
				 * The length is +1 if we're drawing the vertical header bar too
				 */
				int extra = (mVBar && getNumColumns() > 1) ? 1 : 0;
				StringBuffer buf = getUnderlineStr(new StringBuffer(), UNDERLINE_CHAR_DFLT, (mTable[0].length+extra)*Cell.DEFAULT_HPAD);
				System.out.println(buf.toString());
			}
			for (int j = 0; j < mTable[i].length; ++j)
			{
				if (j == 1 && mVBar && getNumRows() > 1)
				{
					/**
					 * Print the vertical bar
					 */
					System.out.print(EDGE_CELL.getContents());
				}
				if (mTable[i][j] == null)
				{
					if (mPrintNull)
					{
						System.out.print(NULL_CELL.getContents());
					}
					else
					{
						System.out.print(EMPTY_CELL.getContents());
					}
				}
				else
				{
					System.out.print(mTable[i][j].getContents());
				}
			}
			System.out.println();
		}
	}
}



/**
 * A Table specifically for displaying user-entered multiplication tables
 */
public class MultiplicationTable extends Table
{
	public static final int MIN_ROWS = 1;
	public static final int MAX_ROWS = 16;
	public static final int MIN_COLUMNS = 1;
	public static final int MAX_COLUMNS = 12;

	/**
	 * This object should only be instantiated after getting user input
	 */
	public MultiplicationTable(int rows, int columns)
	{
		/**
		 * Sanity checking happens in Table, but should add some here too
		 */
		super(rows, columns);
		
		populate(rows, columns);
	}
	
	/**
	 * Populate the multiplication table
	 */
	private void populate(int rows, int columns)
	{
		for (int i = 0; i <= rows; ++i)
		{
			for (int j = 0; j <= columns; ++j)
			{
				if (i == 0)
				{
					if (j == 0)
					{
						// The upper left corner is always empty
						add(new Cell(" "), 0, 0, true);
					}
					else
					{
						// Regular header values
						add(new Cell(j), i, j, true);
					}
				}
				else
				{
					// Add the other cells
					if (j == 0)
					{
						// Add the lefthand header
						add(new Cell(i), i, 0, true);
					}
					else
					{
						// Actual table of values
						add(new Cell(i*j), i, j, true);
					}
				}
			}
		}
	}
	
	/**
	 * Runs various tests on the MultiplicationTables class
	 */
	public static void testMultiplicationTables()
	{
		int table = 0;
		System.out.println("Test table 2 x 2:");
		Table test = new Table(2, 2);
		test.setPrintNull(true);
		test.add(new Cell(1), 0, 0, true);
		test.add(new Cell(2), 0, 1, true);
		test.add(new Cell(3), 1, 0, true);
		test.add(new Cell(4), 1, 1, true);
		test.print(System.out);
		System.out.println("Test " + (++table) + " dimensions: " + test.getNumRows() + " x " + test.getNumColumns() + "\n");
		if (test.getNumRows() != 2 || test.getNumColumns() != 2)
		{
			System.err.println("Test " + table + " failed");
		}

		test.add(new Cell(1), 0, 0, true);
		test.add(new Cell(2), 0, 1, true);
		test.add(new Cell(3), 0, 2, true);
		test.add(new Cell(4), 1, 0, true);
		test.add(new Cell(5), 1, 1, true);
		test.add(new Cell(6), 1, 2, true);
		test.add(new Cell(7), 2, 0, true);
		test.add(new Cell(8), 2, 1, true);
		test.add(new Cell(9), 2, 2, true);
		test.print(System.out);
		System.out.println("Test " + (++table) + " dimensions: " + test.getNumRows() + " x " + test.getNumColumns() + "\n");
		if (test.getNumRows() != 3 || test.getNumColumns() != 3)
		{
			System.err.println("Test " + table + " failed");
		}
		
		test.add(new Cell(10), 3, 0, false); 	// Should increase the size from 3 to 2
		test.print(System.out);
		System.out.println("Test " + (++table) + " dimensions: " + test.getNumRows() + " x " + test.getNumColumns() + "\n");
		if (test.getNumRows() != 4 || test.getNumColumns() != 3)
		{
			System.err.println("Test " + table + " failed");
		}
		
		test.removeRow(1); 	// Should remove the 4 5 6 row
		test.print(System.out);
		System.out.println("Test " + (++table) + " dimensions: " + test.getNumRows() + " x " + test.getNumColumns() + "\n");
		if (test.getNumRows() != 3 || test.getNumColumns() != 3)
		{
			System.err.println("Test " + table + " failed");
		}
		
		test.removeColumn(1);// Should remove the 2 8 column
		test.print(System.out);
		System.out.println("Test " + (++table) + " dimensions: " + test.getNumRows() + " x " + test.getNumColumns() + "\n");
		if (test.getNumRows() != 3 || test.getNumColumns() != 2)
		{
			System.err.println("Test " + table + " failed");
		}
		
		test.removeColumn(2);// Should fail
		test.print(System.out);
		System.out.println("Test " + (++table) + " dimensions: " + test.getNumRows() + " x " + test.getNumColumns() + "\n");
		if (test.getNumRows() != 3 || test.getNumColumns() != 2)
		{
			System.err.println("Test " + table + " failed");
		}
		
		test.removeColumn(1);// Should remove the 3 9 column
		test.print(System.out);
		System.out.println("Test " + (++table) + " dimensions: " + test.getNumRows() + " x " + test.getNumColumns() + "\n");
		if (test.getNumRows() != 3 || test.getNumColumns() != 1)
		{
			System.err.println("Test " + table + " failed");
		}
		
		Cell cell = test.get(2, 0); // Get 10
		if (cell == null || !cell.getValue().equals("10"))
		{
			System.err.println("Test " + (++table) + " failed, value returned: " + ((cell == null) ? cell : cell.getValue()));
		}
		
		cell = test.get(2, 1); // Should fail
		if (cell != null)
		{
			System.err.println("Test " + (++table) + " failed");
		}
		
		cell = test.get(4, 0); // Should fail
		if (cell != null)
		{
			System.err.println("Test " + (++table) + " failed");
		}
		
		cell = test.get(4, 1); // Should fail
		if (cell != null)
		{
			System.err.println("Test " + (++table) + " failed");
		}
		
		test.removeRow(2);// Should remove the 10 row
		test.print(System.out);
		System.out.println("Test " + (++table) + " dimensions: " + test.getNumRows() + " x " + test.getNumColumns() + "\n");
		if (test.getNumRows() != 2 || test.getNumColumns() != 1)
		{
			System.err.println("Test " + table + " failed");
		}
		
		test.removeRow(2);// Should fail
		test.print(System.out);
		System.out.println("Test " + (++table) + " dimensions: " + test.getNumRows() + " x " + test.getNumColumns() + "\n");
		if (test.getNumRows() != 2 || test.getNumColumns() != 1)
		{
			System.err.println("Test " + table + " failed");
		}
		
		int count = test.count(); // Should return 2
		if (count != 2)
		{
			System.err.println("Test " + (++table) + " failed, count() returned: " + count);
		}
		
		System.out.println("Test table -1 x -2:");	// Should result in an empty table
		MultiplicationTable test2 = new MultiplicationTable(-1, -2);
		test2.setPrintNull(true);
		test2.print(System.out);
		System.out.println("Test " + (++table) + " dimensions: " + test2.getNumRows() + " x " + test2.getNumColumns() + "\n");
		if (test2.getNumRows() != 0 || test2.getNumColumns() != 0)
		{
			System.err.println("Test " + table + " failed");
		}
		
		test2.add(new Cell("Invalid Cell"), -1, -1, true); // Should fail
		test2.print(System.out);
		System.out.println("Test " + (++table) + " dimensions: " + test2.getNumRows() + " x " + test2.getNumColumns() + "\n");
		if (test2.getNumRows() != 0 || test2.getNumColumns() != 0)
		{
			System.err.println("Test " + table + " failed");
		}
		
		test2.add(new Cell(1), 0, 0, true);
		test2.add(new Cell(2), 0, 1, true);
		test2.add(new Cell(3), 0, 2, true);
		test2.add(new Cell(4), 1, 0, true);
		test2.add(new Cell(5), 1, 1, true);
		test2.add(new Cell(6), 1, 2, true);
		test2.add(new Cell(7), 2, 0, true);
		test2.add(new Cell(8), 2, 1, true);
		test2.add(new Cell(9), 2, 2, true);
		test2.print(System.out);
		System.out.println("Test " + (++table) + " dimensions: " + test2.getNumRows() + " x " + test2.getNumColumns() + "\n");
		if (test2.getNumRows() != 3 || test2.getNumColumns() != 3)
		{
			System.err.println("Test " + table + " failed");
		}
		
		test2.setPrintHBar(false); // Should print without the horizontal bar
		test2.print(System.out);
		System.out.println("Test " + (++table) + " dimensions: " + test2.getNumRows() + " x " + test2.getNumColumns() + "\n");
		if (test2.getNumRows() != 3 || test2.getNumColumns() != 3)
		{
			System.err.println("Test " + table + " failed");
		}
		
		test2.setPrintVBar(false); // Should print without the vertical bar
		test2.print(System.out);
		System.out.println("Test " + (++table) + " dimensions: " + test2.getNumRows() + " x " + test2.getNumColumns() + "\n");
		if (test2.getNumRows() != 3 || test2.getNumColumns() != 3)
		{
			System.err.println("Test " + table + " failed");
		}
		
		test2.clear();
		test2.print(System.out);
		System.out.println("Test " + (++table) + " dimensions: " + test2.getNumRows() + " x " + test2.getNumColumns() + "\n");
		if (test2.getNumRows() != 3 || test2.getNumColumns() != 3)
		{
			System.err.println("Test " + table + " failed");
		}
	}
	
	/**
	 * Ask the user for a number of rows (1-16) then a number of columns (1-12)
	 * then generate a multiplication table based on their inputs.
	 * When the program is over, display how many tables are generated
	 * @param args	Use "-t" to run a testing version of this program instead
	 */
	public static void main(String[] args)
	{
		if (args.length == 1)
		{
			if (args[0].equals("-t"))
			{
				testMultiplicationTables();
				return;
			}
			else
			{
				System.err.println("Invalid command line argument: " + args[0] + 
								" (only -t for running unit tests is supported)");
			}
		}
		
		final int EXIT = -99;
		int rows, columns, displayed_tables = 0;
		InputParser parser = InputParser.create();
		
		do
		{
			System.out.print("Enter a number of rows 1-16 (-99 to exit): ");
			while (((rows = parser.getInt()) != EXIT) && 
					(rows < MultiplicationTable.MIN_ROWS || rows > MultiplicationTable.MAX_ROWS))
			{
				System.err.println("*** error:  invalid number of rows entered");
				System.out.print("Enter a number of rows 1-16 (-99 to exit): ");
			}
			if (rows == EXIT) break;
			
			System.out.print("Enter a number of columns 1-12 (-99 to exit): ");
			while (((columns = parser.getInt()) != EXIT) && 
					(columns < MultiplicationTable.MIN_COLUMNS || columns > MultiplicationTable.MAX_COLUMNS))
			{
				System.err.println("*** error:  invalid number of columns entered");
				System.out.print("Enter a number of columns 1-12 (-99 to exit): ");
			}
			if (columns == EXIT) break;

			MultiplicationTable mt = new MultiplicationTable(rows, columns);
			mt.print(System.out);
			
			displayed_tables++;
			
			if (!parser.getYesNo("Continue? (Y/N): "))
			{
				break;
			}
		}
		while (true);
		
		System.out.println(displayed_tables + " table(s) generated");
		parser.destroyCloseAll();
		parser = null;
		System.exit(0);
	}
}
