/**
 * @author Adric
 * @created 2014-10-22
 * @course CSC110 MW 3:10PM
 * @due 2014-11-10
 * @version 2014.10.22
 * @brief Create an int array then ask the user if a number is inside
 * 		  the array. Prints % successful guesses at the end. Quits when
 * 		  a user enters -1.
 * 
 * Usage of the works is permitted provided that this instrument is 
 * retained with the works, so that any entity that uses the works 
 * is notified of this instrument.
 * 
 * DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
 * 
 * [2004, Fair License: rhid.com/fair (this URL no longer works)]
 *
 */

import java.util.Random;
import java.util.Scanner;

public class Arrays
{
	private final static int QUIT = -1;
	
	/**
	 * Program Main - loop until the user enters QUIT
	 */
	public static void main(String[] args)
	{
		/*
		 * Using Scanner instead of InputParser so we can
		 * keep track of the fail counts
		 */
		Scanner scanner = new Scanner(System.in);
		int q_count = 0;
		int found_count = 0;
		/*
		 * This assignment uses the default array sizes
		 */
		ArrayRange array_range = new ArrayRange();
		
		System.out.println("Array contents:");
		array_range.print();
		System.out.println();
		
		prompt: do
		{
			System.out.format("Enter a number to search for [%d-%d] (-1 to Quit): ", array_range.getMinRange(), array_range.getMaxRange());
			while (!scanner.hasNextInt())
			{
				/*
				 * Invalid searches count as guesses
				 */
				q_count++;
				System.out.println("Invalid entry");
				scanner.next();
				continue prompt;
			}
			
			int input = scanner.nextInt();
			if (input == QUIT)
			{
				break prompt;
			}
			
			while ((input < array_range.getMinRange()) || (input > array_range.getMaxRange()))
			{
				/*
				 * Invalid searches count as guesses
				 */
				q_count++;
				System.out.println("Invalid range");
				/*
				 * Note: don't need to move to the end of the scanner buffer here
				 */
				continue prompt;
			}
			
			q_count++;
			int found = array_range.count(input);
			if (found != 0)
			{
				System.out.println(input + " was found " + found + " time(s)");
				found_count++;
			}
			else
			{
				System.out.println(input + " not found");
			}
		}
		while (true);
		
		System.out.format("%n%d of %d searches (%s%%) were successful.", found_count, q_count, ((q_count == 0) ? "0" : StringUtils.fToStr((100.f*found_count)/q_count)));
		scanner.close();
		System.exit(0);
	}
}

/**
 * Class containing an array populated by randomly generated values.
 */
class ArrayRange
{
	// Default array size for searching
	private final static int ARRAY_SIZE_DFLT = 16;
	// Default range max (inclusive)
	private final static int MAX_RANGE_DFLT = 42;
	// Default range min
	private final static int MIN_RANGE_DFLT = 0;
	
	private int mArraySize;
	private int mMinRange;
	private int mMaxRange;
	private int[] mArray;
	
	/**
	 * Use all defaults
	 */
	ArrayRange()
	{
		mArraySize = ARRAY_SIZE_DFLT;
		mMinRange = MIN_RANGE_DFLT;
		mMaxRange = MAX_RANGE_DFLT;
		mArray = new int[ARRAY_SIZE_DFLT];
		init();
	}
	
	/**
	 * Specifies values for the array
	 * @param array_size
	 * @param min_range
	 * @param max_range
	 */
	ArrayRange(int array_size, int min_range, int max_range)
	{
		/*
		 * Can't have a negative range since -1 is QUIT
		 */
		if ((min_range >= max_range) || 
			(min_range < 0) || (max_range < 0))
		{
			System.out.format("Invalid range! Using default values: [%d-%d]\n", MIN_RANGE_DFLT, MAX_RANGE_DFLT);
			mMinRange = MIN_RANGE_DFLT;
			mMaxRange = MAX_RANGE_DFLT;
		}
		else
		{
			mMinRange = min_range;
			mMaxRange = max_range;
		}
		if (array_size > 0)
		{
			mArraySize = array_size;
			mArray = new int[mArraySize];
		}
		else
		{
			System.out.format("Invalid array size! Using default: %d\n", ARRAY_SIZE_DFLT);
			this.mArraySize = ARRAY_SIZE_DFLT;
			mArray = new int[ARRAY_SIZE_DFLT];
		}
		init();
	}
	
	/**
	 * Re-populates the class' array variable with random values
	 */
	private void init()
	{
		Random random = new Random(System.currentTimeMillis());
		for (int i = 0; i < mArray.length; ++i)
		{
			/*
			 * Note: checking proper max/min values happens in the ctor
			 */
			mArray[i] = random.nextInt(getMaxRange()-getMinRange()+1) + getMinRange();
		}
	}

	/**
	 * Prints out the contents of the array five numbers
	 * at a time, separated by spaces
	 */
	public void print()
	{
		for (int i = 0; i < mArray.length; ++i)
		{
			System.out.format("%d\t", mArray[i]);
			if ((i+1) % 4 == 0)
			{
				System.out.println();
			}
		}
	}
	
	/**
	 * Search the array for a number
	 * @param num_to_check Number to search for inside the array
	 * @return	Returns 0 if the number is not found,
	 * 			otherwise returns the count in the array.
	 */
	public int count(int num_to_check)
	{
		int found = 0;
		for (int i = 0; i < mArray.length; ++i)
		{
			if (mArray[i] == num_to_check)
			{
				found++;
			}
		}
		return found;
	}
	
	/**
	 * Returns the min range for the numbers in the array (inclusive)
	 */
	public int getMinRange()
	{
		return mMinRange;
	}
	
	/**
	 * Returns the max range for the numbers in the array (inclusive)
	 */
	public int getMaxRange()
	{
		return mMaxRange;
	}
}
