/**
 * @author Adric
 * @created 2015-09-13
 * @course CSC110 MW 3:10PM
 * @due 2014-09-17
 * @version 2014.09.13
 * @brief Performs arithmetic operations on integer inputs
 * 
 * The MIT License (MIT)
 * 
 * Copyright (C) 2014
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

public class Arithmetic
{
	public static enum EOperation
	{
		ADD('+'),
		SUB('-'),
		MULT('*'),
		DIV('/'),
		MOD('%');
		private char symbol;
		EOperation(char symbol)
		{
			this.symbol = symbol;
		}
		
		char symbol()
		{
			return symbol;
		}
	}
	
	private static final int Q_COUNT_DFLT = 4;

	public static void main(String[] args)
	{
		int q_count = 0;
		int max_q_count = Q_COUNT_DFLT;
		if (args.length == 1)
		{
			Integer count = StringUtils.toInteger(args[0]);
			if (count != null && count > 0)
			{
				max_q_count = count;
			}
		}

		/**
		 * Prompt the user at least once
		 */
		InputParser iparser = InputParser.create();
		do
		{
			System.out.print("Enter a non-zero integer: ");
			int val = iparser.getInt();
			if (val == 0)
			{
				System.out.println(val + " is invalid");
				continue;
			}
			
			/**
			 *  Display the following output to the user:
			 * 
			 * 		the opposite of n is y
			 * 		n doubled is y
			 * 		one-half of n is y
			 * 		n squared is y
			 * 		the reciprocal of n is y
			 * 		one-tenth of n is y
			 * 		y squared is z
			 * 		n minus its last digit (d) is y
			 * 		n plus n+1 plus n+2 is y
			 */
			System.out.format("%nThe opposite of %d is %s", val, StringUtils.fToStr(calc(EOperation.MULT, val, -1.f)));
			System.out.format("%n%d doubled is %s", val, StringUtils.fToStr(calc(EOperation.MULT, val, 2.f)));
			System.out.format("%nHalf of %d is %s", val, StringUtils.fToStr(calc(EOperation.DIV, val, 2.f)));
			System.out.format("%n%d squared is %s", val, StringUtils.fToStr(calc(EOperation.MULT, val, val)));
			System.out.format("%nThe reciprocal of %d is %s", val, StringUtils.fToStr(calc(EOperation.DIV, 1.f, val)));
			double tenth = calc(EOperation.DIV, val, 10.f);
			System.out.format("%nOne-tenth of %d is %s", val, StringUtils.fToStr(tenth));
			System.out.format("%nOne-tenth of %d squared is %s", val, StringUtils.fToStr(calc(EOperation.MULT, tenth, tenth)));
			double plus_1 = calc(EOperation.ADD, val, 1.f);
			System.out.format("%n%d plus 1 is %s", val, StringUtils.fToStr(plus_1));
			double plus_2 = calc(EOperation.ADD, val, 2.f);
			System.out.format("%n%d plus 2 is %s", val, StringUtils.fToStr(plus_2));
			// Note: modulo still returns a signed number, hence taking the absolute value here
			double last_digit = calc(EOperation.MOD, val, 10.f);
			if (last_digit < 0.f) last_digit *= -1.f;
			System.out.format("%n%d minus its last digit (%s) is %s", val, StringUtils.fToStr(last_digit), StringUtils.fToStr(calc(EOperation.SUB, val, last_digit)));
			System.out.format("%n%d plus %s plus %s is %s%n%n", val, StringUtils.fToStr(plus_1), StringUtils.fToStr(plus_2), StringUtils.fToStr(calc(EOperation.ADD, val, plus_1+plus_2)));
			q_count++;
		}
		while (q_count < max_q_count);
		iparser.destroyCloseAll();
		iparser = null;
		System.exit(0);
	}
	
	/**
	 * Calculates basic math without using the Math class
	 * @param op		A math EOperation
	 * @param first		First number
	 * @param second 	Second Number
	 * @return 			Returns the result of an operation
	 */
	public static double calc(EOperation op, double first, double second)
	{
		try
		{
			if (!Double.isFinite(first) || !Double.isFinite(second))
			{
				throw new ArithmeticException("Can't operate on NaN or Infinity");
			}
			switch (op)
			{
				case ADD:
					return first+second;
				case MULT:
					// Don't return -0.f
					if (first == 0.f || second == 0.f)
					{
						return 0.f;
					}
					return first*second;
				case SUB:
					return first-second;
				case DIV:
					try
					{
						if (second == 0.f)
						{
							throw new ArithmeticException("Division by 0");
						}
						return first/second;
					}
					catch (ArithmeticException ex)
					{
						System.err.println("Invalid division: " + first + "/" + second + ". " + ex.getMessage());
						return 0.f;
					}
				case MOD:
					return first%second;
				default:
					return 0.f;
			}
		}
		catch (ArithmeticException ex)
		{
			System.err.println("Invalid calculation: " + first + op.symbol() + second + ". " + ex.getMessage());
			return 0.f;
		}
	}
}
