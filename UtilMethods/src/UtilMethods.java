/**
 * @author Adric
 * @created 2014-11-19
 * @course CSC110 MW 3:10PM
 * @due 2014-12-01
 * @brief Demonstrates static class methods
 * 
 * NASA OPEN SOURCE AGREEMENT VERSION 1.3
 * 
 * THIS OPEN SOURCE AGREEMENT ("AGREEMENT") DEFINES THE RIGHTS OF USE,
 * REPRODUCTION, DISTRIBUTION, MODIFICATION AND REDISTRIBUTION OF CERTAIN
 * COMPUTER SOFTWARE ORIGINALLY RELEASED BY THE UNITED STATES GOVERNMENT
 * AS REPRESENTED BY THE GOVERNMENT AGENCY LISTED BELOW ("GOVERNMENT
 * AGENCY"). THE UNITED STATES GOVERNMENT, AS REPRESENTED BY GOVERNMENT
 * AGENCY, IS AN INTENDED THIRD-PARTY BENEFICIARY OF ALL SUBSEQUENT
 * DISTRIBUTIONS OR REDISTRIBUTIONS OF THE SUBJECT SOFTWARE. ANYONE WHO
 * USES, REPRODUCES, DISTRIBUTES, MODIFIES OR REDISTRIBUTES THE SUBJECT
 * SOFTWARE, AS DEFINED HEREIN, OR ANY PART THEREOF, IS, BY THAT ACTION,
 * ACCEPTING IN FULL THE RESPONSIBILITIES AND OBLIGATIONS CONTAINED IN
 * THIS AGREEMENT.
 */

public class UtilMethods
{
	/**
	 * Program main
	 */
	public static void main(String[] args)
	{
		doGrades();
		doZipcodes();
		doRanges();
		System.exit(0);
	}
	
	/**
	 * Ask for grades
	 */
	public static void doGrades()
	{
		final char QUIT = '&';
		char ch = ' ';
		InputParser parser = InputParser.create();
		q_loop: while (true)
		{
			System.out.print("Enter a grade (A, B, C, D, F, W, Y, I, P, Z, & to quit): ");
			if ((ch = parser.getChar()) != QUIT)
			{
				String valid = (isGrade(ch)) ? "valid" : "invalid";
				System.out.println("Grade '" + ch + "' is " + valid);
			}
			else if (confirmQuit())
			{
				break q_loop;
			}
		}
	}
	
	/**
	 * Returns true if a grade is valid
	 * @param ch	Valid grades: A, B, C, D, F, W, Y, I, P, Z (and lowercase)
	 */
	public static boolean isGrade(char ch)
	{
		ch = Character.toUpperCase(ch);
		return ((ch >= 'A' && ch <= 'D') || ch == 'F' || ch == 'W' || 
				 ch == 'Y' || ch == 'I'  || ch == 'P' || ch == 'Z' );
	}

	/**
	 * Ask for zipcodes
	 */
	public static void doZipcodes()
	{
		final int QUIT = 0;
		int zip = -1;
		InputParser parser = InputParser.create();
		q_loop: while (true)
		{
			System.out.print("Enter a five digit zipcode (0 to exit): ");
			if ((zip = parser.getInt()) != QUIT)
			{
				System.out.print("Zipcode '" + zip + "' is ");
				if (inRange(10000, 99999, zip) == -1)
					System.out.println("too short");
				else if (inRange(10000, 99999, zip) == 1)
					System.out.println("too long");
				else
					System.out.println("valid");
			}
			else if (confirmQuit())
			{
				break q_loop;
			}
		}
	}
	
	/**
	 * Returns true if a valid zipcode format was entered.
	 * Note that not all zipcodes in this range are actually zipcodes,
	 * this was just the range given by the assignment.
	 */
	public static boolean isZipcode(int zip)
	{
		return (Integer.toString(zip).length() == 5);
	}

	/**
	 * Ask for ranges
	 */
	public static void doRanges()
	{
		InputParser input = InputParser.create();
		int min, max, val = 0;
		q_loop: while (true)
		{
			System.out.print("Enter minimum integer range value: ");
			min = input.getInt();
			System.out.print("Enter a maximum integer range value: ");
			max = input.getInt();
			System.out.print("Enter integer value (min to exit): ");
			val = input.getInt();
			if (val != min)
			{
				if (min > max)
				{
					System.out.println("Must enter a minimum less than a maximum");
					continue q_loop;
				}
				switch(inRange(min, max, val))
				{
					case -1:
						System.out.println(val + " is less than minimum value " + min);
						break;
					case 0:
						System.out.println(val + " is between " + min + " and " + max);
						break;
					case 1:
						System.out.println(val + " is greater than maximum value " + max);
						break;
					default:
						System.err.println("Invalid return type");
						break;
				}
			}
			else if (confirmQuit())
			{
				break q_loop;
			}
		}
	}
	
	/**
	 * Returns -1 if the value is less than the minimum value. 
	 * Returns 0 if the value is greater than or equal to the minimum value, 
	 * or the value is less than or equal to the maximum value. 
	 * Returns 1 if the value is greater than the maximum value.
	 */
	public static int inRange(int min, int max, int val)
	{
		return ((val < min) ? -1 : 
				(val > max) ?  1 : 0);
	}
	
	/**
	 * Prompts the user to enter y/n to exit.
	 * Returns true if the user entered 'y', false if 'n'.
	 */
	private static boolean confirmQuit()
	{
		InputParser parser = InputParser.create();
		while (true)
		{
			System.out.print("Are you sure you want to exit? (Y/N): ");
			switch (Character.toUpperCase(parser.getChar()))
			{
				case 'N':
					return false;
				case 'Y':
					return true;
				default:
					System.out.println("Must enter a valid character");
					break;
			}
		}
	}
}
