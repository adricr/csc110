/**
 * @author Adric Rukkila
 * @created 2014-10-13
 * @course CSC110 MW 3:10PM
 * @due 2014-10-27
 * @version 2014.10.14
 * @brief Asks a user to input a password, then gives it a score
 * 		  based on how strong or weak it is
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public 
 * License along with this program.  If not, see 
 * <http://www.gnu.org/licenses/>.
 */

public class Password
{
	private String mPass;
	private static final String QUIT = "Q";
	
	/**
	 * Loop until the user enters QUIT
	 */
	public static void main(String[] args)
	{
		InputParser parser = InputParser.create();
		while (true)
		{
			System.out.print("Enter a password (or \"" + QUIT + "\" to quit): ");
		    String input = parser.getString();
		    if (input.toUpperCase().equals(QUIT))
		    {
		    	break;
		    }
		    Password password = new Password(input);
		    System.out.format("\"%s\" has a score of: %d\n", input, password.getScore());
		}
		parser.destroyCloseAll();
		parser = null;
		System.exit(0);
	}
	
	/**
	 * User-entered password (can be empty)
	 */
	public Password(String password)
	{
		mPass = password;
	}
	
	private boolean isUpperCase(char ch)
	{
		return ((ch >= 'A') && (ch <= 'Z'));
	}
	
	private boolean isLowerCase(char ch)
	{
		return ((ch >= 'a') && (ch <= 'z'));
	}
	
	private boolean isDigit(char ch)
	{
		return ((ch >= '0') && (ch <= '9'));
	}
	
	private boolean isPunct(char ch)
	{
		return ((ch >= '!') && (ch <= '/') || 
				(ch >= ':') && (ch <= '@') || 
				(ch >= '[') && (ch <= '`') || 
				(ch >= '{') && (ch <= '~'));
	}
	
	private boolean isMostUsed(String str)
	{
		/*
		 * Source: 
		 * http://www.zdnet.com/article/25-most-used-passwords-revealed-is-yours-one-of-them/
		 * Contains both forwards and backwards versions
		 */
		String[] most_used = { 
				"password", "123456", 	"12345678", "qwerty", 
				"baseball", "football", "letmein", 	"111111", 
				"superman", "master", 	"harley", 	"yelrah", 
				"retsam", 	"namrepus", "111111", 	"niemtel", 
				"llabtoof", "llabesab", "ytrewq", 	"87654321", 
				"654321", 	"drowssap" };
		for (String s : most_used)
		{
			if (s.equals(str))
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Generate a password score based on the following rules:
	 * 	+1 if length is six or greater.
	 * 	+1 if at least one uppercase character.
	 * 	+1 if at least one lowercase character.
	 * 	+1 if at least one digit.
	 * 	+1 if at least one punctuation character.
	 * 	-5 if a common password (forwards or backwards)
	 * @return Returns int score based on password strength
	 */
	public int getScore()
	{
		int score = 0;
		if (mPass == null || mPass.isEmpty())
		{
			return score;
		}
		
		/*
		 * Most used is minus 5
		 */
		if (isMostUsed(mPass))
		{
			score -= 5;
		}
		
		/*
		 * Complexity - add 1 for each rule found
		 */
		boolean has_upper = false;
		boolean has_lower = false;
		boolean has_punct = false;
		boolean has_digit = false;
		for (int i = 0 ; i < mPass.length(); ++i)
		{
			if (!has_upper && isUpperCase(mPass.charAt(i)))
			{
				score++;
				has_upper = true;
			}
			else if (!has_lower && isLowerCase(mPass.charAt(i)))
			{
				score++;
				has_lower = true;
			}
			else if (!has_punct && isPunct(mPass.charAt(i)))
			{
				score++;
				has_punct = true;
			}
			else if (!has_digit && isDigit(mPass.charAt(i)))
			{
				score++;
				has_digit = true;
			}
		}
		
		/*
		 * Length is just worth 1 (aww!)
		 */
		if (mPass.length() > 5)
		{
			score++;
		}
		
		return score;
	}
}
