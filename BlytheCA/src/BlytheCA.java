/**
 * @author Adric
 * @created 2014-10-22
 * @course CSC110 MW 3:10PM
 * @due 2014-11-05
 * @version 2014.11.04
 * @brief Converts directions from AZ to Blythe into multiple bases
 * 
 * The contents of this file are subject to the Common Public Attribution License
 * Version 1.0 (the �License�); you may not use this file except in compliance 
 * with the License. You may obtain a copy of the License at 
 * https://spdx.org/licenses/CPAL-1.0. The License is based on the Mozilla Public 
 * License Version 1.1 but Sections 14 and 15 have been added to cover use of 
 * software over a computer network and provide for limited attribution for the 
 * Original Developer. In addition, Exhibit A has been modified to be consistent 
 * with Exhibit B.
 * 
 * Software distributed under the License is distributed on an �AS IS� basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
 * the specific language governing rights and limitations under the License.
 * 
 * The Original Code is BlytheCA.java.
 * The Initial Developer of the Original Code is Adric Rukkila. All portions of 
 * the code written by Adric Rukkila are Copyright (c) 2014. All Rights Reserved.
 */

import java.util.Vector;

public class BlytheCA
{
	/**
	 * Program main
	 */
	public static void main(String[] args)
	{
		/*
		 * Print directions in base-10, binary, octal, and hex:
		 */
		System.out.println("Driving directions between Tempe, Arizona and Blythe, California:");
		int[] bases = new int[5];
		bases[0] = 10;
		bases[1] = 2;
		bases[2] = 8;
		bases[3] = 16;
		bases[4] = 26;  // And why not base-26?
		for (int i : bases)
		{
			System.out.println("\nIn base-" + i + ":");
			System.out.println(" * From Guadalupe and Price roads in Tempe, drive north " + BlytheCA.toBaseString(6, i) + " miles on Loop-" + BlytheCA.toBaseString(101, i));
			System.out.println(" * Go west about " + BlytheCA.toBaseString(10, i) + " miles on Loop-" + BlytheCA.toBaseString(202, i) + " until it merges into I-" + BlytheCA.toBaseString(10, i));
			System.out.println(" * Head west on I-" + BlytheCA.toBaseString(10, i) + " for approximately " +BlytheCA.toBaseString(146, i) + " miles to Blythe, California");
		}
		System.exit(0);
	}
	
	/**
	 * Converts a number to base-10 from a different base
	 * @param num		Number a string
	 * @param old_base	Original base of num
	 * @return			Returns the converted number as a long
	 */
	public static long toBase10(String num, int old_base)
	{
		// TODO: figure out how to handle negative/out of range bases
		long rv = 0;
		for (int i = num.length()-1, power = 0; i >= 0; --i)
		{
			if (num.charAt(i) == '-')
			{
				rv *= -1;
				break;
			}

			if (old_base > 10)
			{
				/*
				 * alphanum - Character.getNumericValue returns 'A' == 10, etc.
				 */
				rv += (Character.getNumericValue(num.charAt(i))) * (int)Math.pow(old_base, power);
			}
			else
			{
				rv += Integer.parseInt(num.substring(i, i+1)) * (int)Math.pow(old_base, power);
			}
			power++;
		}
		return rv;
	}
	
	/**
	 * Converts a number from base-10 to bases 2-26
	 * @param num		Number an int (signed)
	 * @param new_base	Base to convert to
	 * @return			Returns a number in the new base a string
	 */
	public static String toBaseString(int num, int new_base)
	{
		if (new_base < 2 || new_base > 26)
		{
			System.out.println("Can only convert bases 2-26");
			return null;
		}

		StringBuffer str = new StringBuffer();
		Vector<Long> places = new Vector<Long>();		
		long result = num;

		/*
		 * Converts an integer to a new base by getting the remainder then dividing by the base. 
		 * For example, the number 4 converted from base-10 to binary:
		 * 	4 % 2 == 0
		 *  4 / 2 == 2
		 *	2 % 2 == 0
		 *	2 / 2 == 1
		 *	1 % 2 == 1
		 *	1 / 2 == 0 END
		 * Final binary string: "100"
		 */
		while (result != 0)
		{
			places.add(result % new_base);
			result /= new_base;
		}
		
		/*
		 * Build the new number string from right-to-left. 
		 * When the base is > 10, use letters for 10+, knowing that:
		 * ASCII 'A' == 65
		 * ASCII 'Z' == 90
		 * ('a' == 97 and 'z' == 122 if we ever want to use lowercase)
		 */
		final int LETTER_OFFSET = 55;
		for (int i = places.size()-1; i >= 0; --i)
		{
			if (new_base > 10L && new_base <= 26L && places.get(i) >= 10L)
			{
				str.append((char)(places.get(i)+LETTER_OFFSET));
			}
			else
			{
				str.append(Long.toString(places.get(i) % new_base));
			}
		}
		return str.toString();
	}
}