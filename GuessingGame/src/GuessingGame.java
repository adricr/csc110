/**
 * @author Adric
 * @created 2014-10-22
 * @course CSC110 MW 3:10PM
 * @due 2014-11-17
 * @version 2014.11.03
 * @brief A game where players guess random numbers in a range
 * 
 ***********************************************************************
 * Copyright (c) 2014 Adric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 ***********************************************************************
 */

import java.util.Random;
import java.util.Vector;

public class GuessingGame
{
	/**
	 * Program main. Ask a player for their player name, if not passed via command line
	 */
	public static void main(String[] args)
	{
		if (args.length > 1)
		{
			/**
			 * Starts a new game with a new name and autoplay, if passed "a"
			 */
			new Game(args[0], (args[1].toLowerCase().equals("a")));
		}
		else if (args.length > 0)
		{
			/**
			 * Starts a new game with a new name (autplays)
			 */
			System.out.println("args index 0: " + args[0]);
			new Game(args[0], true);
		}
		else
		{
			/**
			 * Starts a new game after asking the player to enter their name (autoplays)
			 */
			InputParser parser = InputParser.create();
			System.out.print("Enter your name: ");
			new Game(parser.getLine(), true);
			parser.destroyCloseAll();
			parser = null;
		}
		System.exit(0);
	}
}

/**
 * The game itself!
 * 
 * Uses class Random to generate a random number between 1 and 110 (inclusive). 
 * Prompts the player to enter a guess. 
 * Loops until the player guesses the random number or enters a sentinel value to give up. 
 * Prints an error message if the player enters a wrong guess more than once. 
 * After a game has been completed prompt the player to see if they want to play again. 
 */
class Game
{
	/**
	 * Limits
	 */
	private static final int MIN_NUM = 1;
	private static final int MAX_NUM = 110;
	private static final int QUIT = -1;
	private static final int MAX_GAMES = 256;
	
	/**
	 * Player stats
	 */
	private String m_PlayerName;
	private int m_GamesPlayed;
	private int m_GamesWon;
	private int m_RightAnswer;
	private int m_GuessCount;
	private Vector<Integer> m_Guesses;
	
	Game(String player, boolean autoplay)
	{
		m_PlayerName = player;
		m_Guesses = new Vector<Integer>();
		
		initNum();
		
		if (autoplay)
		{
			play();
		}
	}
	
	/**
	 * Generate a new random number to guess
	 */
	private void initNum()
	{
		/**
		 * This will always get us the correct range when max > min
		 * The check for that happens elsewhere
		 */
		Random rand = new Random(System.currentTimeMillis());
		m_RightAnswer = rand.nextInt(MAX_NUM - MIN_NUM + 1) + MIN_NUM;
	}
	
	/**
	 * Play the game! Rules:
	 * 
	 * Duplicate wrong guesses count as only one wrong guess. 
	 * After five wrong guesses, the player is given help (higher or lower messages). 
	 * After a game has been completed prompt the player to see if they want to play again. 
	 * The player is allowed to play at most 256 games. 
	 */
	public void play()
	{
		InputParser parser = InputParser.create();
		/**
		 * Reset any games played, won, or guesses
		 */
		m_GamesPlayed = 0;
		m_GamesWon = 0;
		m_GuessCount = 0;
		m_Guesses.clear();
		
		/**
		 * Game loop
		 */
		System.out.println("*** Welcome to the CSC110 Guessing Game, " + m_PlayerName + " ***");
		do
		{
			/**
			 * Quit if they've reached max games
			 */
			m_GamesPlayed++;
			if (m_GamesPlayed >= MAX_GAMES)
			{
				System.out.println("You played " + m_GamesPlayed + " games! Cannot play anymore");
				break;
			}
			
			/**
			 * Ask the player for a number, or if they want to quit
			 */
			System.out.print("Guess a number between " + MIN_NUM + " and " + MAX_NUM + " (" + QUIT + " to give up): ");
			int guess = parser.getInt();
			if (guess == QUIT)
			{
				System.out.println("*** QUITTER ***");
				break;
			}
			/**
			 * Print an error message if the player enters a number that is not between MIN_NUM and MAX_NUM. 
			 */
			else if (guess < MIN_NUM)
			{
				System.out.println(guess + " is too small...");
				m_GuessCount++;
				continue;
			}
			else if (guess > MAX_NUM)
			{
				System.out.println(guess + " is too large...");
				m_GuessCount++;
				continue;
			}
			else
			{
				/**
				 * A valid guess
				 */
				m_GuessCount++;
			}

			/**
			 * Check their guess.
			 * Duplicates wrong guesses count as only one wrong guess. 
			 * After five wrong guesses, the player is given help (higher or lower messages). 
			 */ 
			if (guess == m_RightAnswer)
			{
				/**
				 * Correct!
				 */
				m_GamesWon++;
				System.out.println("*** GOT IT *** it took you " + m_GuessCount + " guesses");
				/**
				 * Ask if they want to play again
				 */
				if (parser.getYesNo("Do you want to play again? (Y/N): "))
				{
					/**
					 * Generate a new number, reset guesses, then play again
					 */
					initNum();
					m_GuessCount = 0;
					m_Guesses.clear();
				}
				else
				{
					/**
					 * Quit the game
					 */
					System.out.println("*** QUITTER ***");
					break;
				}
			}
			else
			{
				/**
				 * Wrong! Check for duplicate guesses.
				 */
				if (m_Guesses.contains(guess))
				{
					/**
					 * If the guess is a duplicate, don't count it against them
					 */
					System.out.println("What part of \"nope\" don't you understand?");
					m_GuessCount--;
					continue;
				}
				else
				{
					/**
					 * Add it to the guesses list
					 */
					m_Guesses.addElement(guess);
					System.out.print("Nope");
					/**
					 * Give the player a hint if they've made 5 or more guesses
					 */
					if (m_GuessCount >= 5)
					{
						if (guess > m_RightAnswer)
						{
							System.out.print("...lower");
						}
						else
						{
							System.out.print("...higher");
						}
					}
					System.out.println("!");
				}
			}
		}
		while (true);
		
		/**
		 * Print out stats at the end
		 */
		System.out.println("Thank you for playing, " + m_PlayerName);
		System.out.format("%nYou played %d games and won %d (win percentage: %s%%)", m_GamesPlayed, m_GamesWon, StringUtils.fToStr(m_GamesWon*100.f/m_GamesPlayed));
	}
}
