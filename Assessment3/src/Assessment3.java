/**
 * @author Adric Rukkila
 * @created 2014-11-19
 * @course CSC110 MW 3:10PM
 * @due 2014-11-24
 * @brief Prints CSC110 Asessment3 answers
 */

public class Assessment3
{
	public static void main(String[] args)
	{
		char[] answers = new char[20];
		answers[0] = 'B';
		answers[1] = 'B';
		answers[2] = 'B';
		answers[3] = 'A';
		answers[4] = 'B';
		answers[5] = 'B';
		answers[6] = 'A';
		answers[7] = 'E';
		answers[8] = 'A';
		answers[9] = 'B';
		answers[10] = 'C';
		answers[11] = 'D';
		answers[12] = 'C';
		answers[13] = 'C';
		answers[14] = 'C';
		answers[15] = 'B';
		answers[16] = 'B';
		answers[17] = 'B';
		answers[18] = 'B';
		answers[19] = 'E';
		for (char ch : answers)
		{
			System.out.print(ch + " ");
		}
	}
}
