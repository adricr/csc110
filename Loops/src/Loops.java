/**
 * @author Adric
 * @created 2015-09-22
 * @course CSC110 MW 3:10PM
 * @due 2014-10-08
 * @version 2014.09.29
 * @brief Use 3 different loop types to print a series of integers
 * 
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * Adric wrote this file. As long as you retain this notice you can do whatever 
 * you want with this stuff. If we meet some day, and you think this stuff is 
 * worth it, you can buy me a beer in return.
 * ----------------------------------------------------------------------------
 */

public class Loops
{
	private static int L_COUNT_DFLT = 10;

	/**
	 * Program main
	 * @param args
	 */
	public static void main(String[] args)
	{
		int loop_count = L_COUNT_DFLT;
		if (args.length == 1)
		{
			Integer count = StringUtils.toInteger(args[0]);
			if (count != null && count > 0)
			{
				loop_count = count;
			}
		}
		loop(loop_count);
	}
	
	/**
	 * Performs three loops: for, do-while, while, then prints the results
	 * @param loop_count Number of loops. Must be a positive integer
	 */
	public static void loop(final int loop_count)
	{
		/**
		 * Expected output for a loop count of 10:
		 * 
		 *  2,4,6,8,10,12,14,16,18,20,
		 *  19,17,15,13,11,9,7,5,3,1,
		 *  0,2,6,12,20,30,42,56,72,90,
		 */
		if (loop_count <= 0)
		{
			System.err.format("Invalid loop count: %d. Must be a positive integer!", loop_count);
			return;
		}
		
		int[][] series = new int[3][loop_count];
		int loop_type = 0;
		int i = 0;
		
		/**
		 * for loop - evens in ascending order
		 */
		for ( ; i < loop_count; ++i)
		{
			series[loop_type][i] = (i + 1) * 2;
		}
		loop_type++;
		
		/**
		 * do-while loop - odds in descending order
		 */
		i = loop_count;
		int temp = 0;
		do
		{
			series[loop_type][temp++] = (i * 2) - 1;
			i--;
		}
		while (i > 0);
		loop_type++;
		
		/**
		 * while loop - consecutive n(n+1) numbers starting at 0
		 */
		i = 0;
		while (i < loop_count)
		{
			series[loop_type][i] = i*(i+1);
			i++;
		}
		
		printMultiArray(series);
	}
	
	/**
	 * Prints an array of arrays, each as a csv line
	 * @param array Multidimensional array int[][]
	 */
	public static void printMultiArray(int[][] array)
	{
		for (int[] a : array)
		{
			printArray(a);
		}
	}
	
	/**
	 * Print an individual array as a csv line
	 * @param array Integer array to print
	 */
	public static void printArray(int[] array)
	{
		if (array.length < 1)
		{
			System.out.println("Empty array, nothing to print!");
			return;
		}
		
		StringBuffer output = new StringBuffer();
		for (int i = 0; i < array.length; ++i)
		{
			output.append(array[i]);
			if (i < (array.length - 1))
			{
				output.append(",");
			}
		}
		System.out.println(output);
	}
}
