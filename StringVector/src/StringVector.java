/**
 * @author Adric
 * @created 2014-10-22
 * @course CSC110 MW 3:10PM
 * @due 2014-12-08
 * @version 2014.11.20
 * @brief Specialized vector of strings that prints out
 * 		  its contents in various ways
 * 
 * Licensed under the Academic Free License version 3.0
 */

import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

/**
 * A special String Vector which can print its content in multiple variations
 */
@SuppressWarnings("serial")
public class StringVector extends Vector<String>
{
	/**
	 * Program Main
	 */
	public static void main(String[] args)
	{
		StringVector sv = new StringVector();
		InputParser parser = InputParser.create();
		while (true)
		{
			System.out.print("Enter a string: ");
			/**
			 * InputParser does the trimming
			 */
			sv.add(parser.getString().toLowerCase());
			if (!confirmContinue())
			{
				break;
			}
		}

		System.out.println("\nStrings entered (lower-cased and trimmed):");
		sv.print();
		System.out.println("\nReversed order:");
		sv.printReverse();
		System.out.println("\nRandom order:");
		sv.printRand();
		System.out.println("\nLongest string(s):");
		sv.printLongest();
		System.out.println("\nShortest string(s):");
		sv.printShortest();
		System.out.println("\nJava keyword(s):");
		sv.printKeywords();
		parser.destroyCloseAll();
		parser = null;
		System.exit(0);
	}
	
	/**
	 * Prompts the user to enter y/n to continue entering strings.
	 * Returns true if the user entered 'y', false if 'n'.
	 */
	private static boolean confirmContinue()
	{
		InputParser parser = InputParser.create();
		while (true)
		{
			System.out.print("Do you want to enter another string? (Y/N): ");
			switch (Character.toUpperCase(parser.getChar()))
			{
				case 'N':
					return false;
				case 'Y':
					return true;
				default:
					System.out.println("Must enter a valid character");
					break;
			}
		}
	}
	
	/**
	 * Prints the vector's elements from first to last
	 */
	public final void print()
	{
		for (int i = 0; i < size(); ++i)
		{
			System.out.println(get(i));
		}
	}
	
	/**
	 * Prints the vector's elements from last to first
	 */
	public final void printReverse()
	{
		for (int i = size(); --i >= 0; )
		{
			System.out.println(get(i));
		}
	}
	
	/**
	 * Prints the vector's contents that match Java keywords
	 */
	public final void printKeywords()
	{
		/**
		 * From: http://docs.oracle.com/javase/tutorial/java/nutsandbolts/_keywords.html
		 * Last checked: November 2014
		 */
		final String[] keywords = { "abstract", "continue", "for", "new", "switch",
									"assert", "default", "goto", "package", "synchronized",
									"boolean", "do", "if", "private", "this",
									"break", "double", "implements", "protected", "throw",
									"byte", "else", "import", "public", "throws",
									"case", "enum", "instanceof", "return", "transient",
									"catch", "extends", "int", "short", "try",
									"char", "final", "interface", "static", "void",
									"class", "finally", "long", "strictfp", "volatile",
									"const", "float", "native", "super", "while" };
		for (int i = 0; i < size(); ++i)
		{
			for (int j = 0; j < keywords.length; ++j)
			{
				if (get(i) != null && get(i).equals(keywords[j]))
				{
					System.out.println(get(i));
					break;
				}
			}
		}
	}
	
	/**
	 * Prints the vector's elements in random order
	 */
	public final void printRand()
	{
		ArrayList<Integer> grabbed = new ArrayList<Integer>(size());
		Random rand = new Random(System.currentTimeMillis());
		int r = rand.nextInt(size());
		for (int i = 0; i < size(); ++i)
		{
			while (grabbed.contains(r))
			{
				r = rand.nextInt(size());
			}
			grabbed.add(r);
		}
		for (int i : grabbed)
		{
			System.out.println(elementAt(i));
		}
	}
	
	/**
	 * Prints the vector's element(s) with the shortest length
	 */
	public final void printShortest()
	{
		int smallest_len = Integer.MAX_VALUE;
		for (int i = 0; i < size(); ++i)
		{
			if (get(i).length() < smallest_len)
			{
				smallest_len = get(i).length();
			}
		}
		for (int i = 0; i < size(); ++i)
		{
			if (get(i) != null && get(i).length() == smallest_len)
			{
				System.out.println(get(i));
			}
		}
	}
	
	/**
	 * Prints the vector's element(s) with the longest length
	 */
	public final void printLongest()
	{
		int largest_len = 0;
		for (int i = 0; i < size(); ++i)
		{
			if (get(i).length() > largest_len)
			{
				largest_len = get(i).length();
			}
		}
		for (int i = 0; i < size(); ++i)
		{
			if (get(i) != null && get(i).length() == largest_len)
			{
				System.out.println(get(i));
			}
		}
	}
}
